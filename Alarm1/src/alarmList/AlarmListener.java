package alarmList;

import AlarmEvent.PinEvent;

public interface AlarmListener {
	public void TurnedON(PinEvent event);
	public void TurnedOFF(PinEvent event);
}
