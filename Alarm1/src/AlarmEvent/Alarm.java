package AlarmEvent;

import java.util.List;
import java.util.ArrayList;

import alarmList.AlarmListener;

public class Alarm {
	public final String PIN = "1234";
	public List<AlarmListener> listeners = new ArrayList<AlarmListener>();
	
	void addListener(AlarmListener listener)
	{
		this.listeners.add(listener);
	}
	void removeListener(AlarmListener listener)
	{
		this.listeners.remove(listener);
	}
	public void enterPIN(String Password)
	{ 
		System.out.println("Give me a password ");
		if(Password == this.PIN)
		{
			this.correctPIN();
		}
		else
		{
			this.wrongPIN();
		}
	}
	void wrongPIN()
	{
		for(AlarmListener x : this.listeners)
		{
			x.TurnedON(new PinEvent(this));
		}
	}
	void correctPIN()
	{
		for(AlarmListener x : this.listeners)
		{
			x.TurnedOFF(new PinEvent(this));
		}
	}
}
