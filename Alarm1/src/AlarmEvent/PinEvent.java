package AlarmEvent;

import java.util.Date;

public class PinEvent {
	Alarm alarm;
	Date eventDate;
	
	public PinEvent(Alarm alarm) {
        this.alarm = alarm;
        this.eventDate = new Date();
    }
	
	Alarm getAlarm()
	{
		return this.alarm;
	}
	Date getDate()
	{
		return this.eventDate;
	}
	
}
